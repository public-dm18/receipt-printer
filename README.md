# Intro

This was a code test. Asked to calculate tax applied to a purchase order according to "by-location tax rates" and "by-location 'list of tax exemption by product types'". Input format and Configuration flow unspecified.  

# Pre-requisites

Java17
https://www.oracle.com/java/technologies/downloads/#java17

# Usage 

```
git clone https://gitlab.com/public-dm18/receipt-printer.git
cd receipt-printer
./gradlew test --rerun
```

# Example Output

```
ReceiptPrintingServiceTest > generateReceiptCase1() STANDARD_OUT
    link to case file= ./src/test/tmp/case1.html

ReceiptPrintingServiceTest > generateReceiptCase2() STANDARD_OUT
    link to case file= ./src/test/tmp/case2.html

ReceiptPrintingServiceTest > generateReceiptCase3() STANDARD_OUT
    link to case file= ./src/test/tmp/case3.html
```

# How-to: Modify Input

Since this is desired to be just a draft/prototype, 
inputs for test cases are hardcoded in test classes in the following fashion:

```
	//case1

        List<OrderedItem> orderedItems = List.of(
                new OrderedItem(new Product("book", "solid", new BigDecimal("17.99")), 1),
                new OrderedItem(new Product("potato chips", "food", new BigDecimal("3.99")), 1)
        );

        Order order = new Order(orderedItems, "CA");
```

Users may modify existing test data or add extra test cases in the same fashion as the existing ones in 

```
./src/test/java/com/codetest/receiptprinter/services/ReceiptPrintingServiceTest.java
```
and then rerun
```
./gradlew test --rerun
```


