package com.codetest.receiptprinter.services;

import com.codetest.receiptprinter.domains.Order;
import com.codetest.receiptprinter.domains.OrderedItem;
import com.codetest.receiptprinter.domains.Product;
import freemarker.template.TemplateException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@SpringBootTest
class ReceiptPrintingServiceTest {

    @Autowired
    ReceiptPrintingService receiptPrintingService;

    @Test
    void generateReceiptCase1() throws TemplateException, IOException {

        List<OrderedItem> orderedItems = List.of(
                new OrderedItem(new Product("book", "solid", new BigDecimal("17.99")), 1),
                new OrderedItem(new Product("potato chips", "food", new BigDecimal("3.99")), 1)
        );

        Order order = new Order(orderedItems, "CA");

        Path path = Paths.get( "src/test/tmp/case1.html");
        try (FileWriter writer = new FileWriter(new File(path.toUri()))){
            String html = receiptPrintingService.generateReceipt(order);
            writer.write(html);
            System.out.println("link to case file= " + path.toAbsolutePath());
        }
    }

    @Test
    void generateReceiptCase2() throws TemplateException, IOException {

        List<OrderedItem> orderedItems = List.of(
                new OrderedItem(new Product("book", "solid", new BigDecimal("17.99")), 1),
                new OrderedItem(new Product("pencil", "solid", new BigDecimal("2.99")), 3)
        );

        Order order = new Order(orderedItems, "NY");

        Path path = Paths.get( "src/test/tmp/case2.html");
        try (FileWriter writer = new FileWriter(new File(path.toUri()))){
            String html = receiptPrintingService.generateReceipt(order);
            writer.write(html);
            System.out.println("link to case file= " + path.toAbsolutePath());
        }

    }

    @Test
    void generateReceiptCase3() throws TemplateException, IOException {

        List<OrderedItem> orderedItems = List.of(
                new OrderedItem(new Product("pencil", "solid", new BigDecimal("2.99")), 2),
                new OrderedItem(new Product("shirt", "clothing", new BigDecimal("29.99")), 1)
        );

        Order order = new Order(orderedItems, "NY");

        Path path = Paths.get( "src/test/tmp/case3.html");
        try (FileWriter writer = new FileWriter(new File(path.toUri()))){
            String html = receiptPrintingService.generateReceipt(order);
            writer.write(html);
            System.out.println("link to case file= " + path.toAbsolutePath());
        }
    }
}