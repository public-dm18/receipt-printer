package com.codetest.receiptprinter.utils;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class BigDecimalUtilsTest {

    @Test
    void roundUpTo5Cent() {

        BigDecimal case1 = new BigDecimal("1.13");
        BigDecimal case2 = new BigDecimal("1.16");
        BigDecimal case3 = new BigDecimal("1.151");
        assertEquals(new BigDecimal("1.15"), BigDecimalUtils.roundUpTo5Cent(case1));
        assertEquals(new BigDecimal("1.20"), BigDecimalUtils.roundUpTo5Cent(case2));
        assertEquals(new BigDecimal("1.20"), BigDecimalUtils.roundUpTo5Cent(case3));
    }
}