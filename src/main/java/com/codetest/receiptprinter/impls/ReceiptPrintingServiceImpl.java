package com.codetest.receiptprinter.impls;

import com.codetest.receiptprinter.domains.Order;
import com.codetest.receiptprinter.domains.OrderedItem;
import com.codetest.receiptprinter.domains.Receipt;
import com.codetest.receiptprinter.domains.ReceiptSummaryTmp;
import com.codetest.receiptprinter.services.ReceiptPrintingService;
import com.codetest.receiptprinter.utils.BigDecimalUtils;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class ReceiptPrintingServiceImpl implements ReceiptPrintingService {

    @Autowired
    Map<String, BigDecimal> taxRateByLocationMap;

    @Autowired
    Map<String, Set<String>> taxExemptProductSetByLocationMap;

    @Autowired
    Template receiptHTTPTableTemplate;


    @Override
    public String generateReceipt(Order order) throws TemplateException, IOException {
        return generateReceipt(order.getItems(), order.getPurchaseLocation());
    }

    private String generateReceipt(List<OrderedItem> orderedItems, String location) throws TemplateException, IOException {

        BigDecimal taxRate = getTaxRateBy(location);
        Set<String> taxExemptionSet = getTaxExemptionSetBy(location);

        // init temporary object for calculation
        ReceiptSummaryTmp receiptSummaryTmp = ReceiptSummaryTmp.newInstance();

        // update receipt summary by each item in the Order
        for (OrderedItem item : orderedItems) {
            BigDecimal itemTotal = item.getItemTotal();
            receiptSummaryTmp.setSubTotal(
                    receiptSummaryTmp.getSubTotal().add(itemTotal)
            );
            if (isTaxable(item, taxExemptionSet)) {
                BigDecimal taxRounded = BigDecimalUtils.roundUpTo5Cent(itemTotal.multiply(taxRate));
                receiptSummaryTmp.setTaxAppliedTotal(
                        receiptSummaryTmp.getTaxAppliedTotal().add(taxRounded)
                );
            }
        }

        StringWriter writer = new StringWriter();
        receiptHTTPTableTemplate.process(
                new Receipt(
                        orderedItems,
                        receiptSummaryTmp.getSubTotal(),
                        receiptSummaryTmp.getTaxAppliedTotal(),
                        receiptSummaryTmp.getSubTotal().add(receiptSummaryTmp.getTaxAppliedTotal())),
                writer);
        return writer.toString();

    }


    private Set<String> getTaxExemptionSetBy(String location) {
        return taxExemptProductSetByLocationMap.get(location);
    }

    private BigDecimal getTaxRateBy(String location) {
        return taxRateByLocationMap.get(location);
    }

    private boolean isTaxable(OrderedItem i, Set<String> taxExemptionSet) {
        return taxExemptionSet.contains(i.getProduct().getType()) == false;
    }

}
