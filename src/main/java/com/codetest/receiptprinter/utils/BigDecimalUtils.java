package com.codetest.receiptprinter.utils;

import lombok.extern.apachecommons.CommonsLog;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;


public class BigDecimalUtils {
    private BigDecimalUtils(){
        throw new UnsupportedOperationException();
    }

    private static BigDecimal FiveCent = new BigDecimal("0.05");

    public static BigDecimal roundUpTo5Cent(BigDecimal originalValue) {
        return originalValue
                .divide(FiveCent, 0, RoundingMode.UP)
                .multiply(FiveCent);
    }


}
