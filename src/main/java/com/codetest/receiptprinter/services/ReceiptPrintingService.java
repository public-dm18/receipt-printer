package com.codetest.receiptprinter.services;


import com.codetest.receiptprinter.domains.Order;
import freemarker.template.TemplateException;

import java.io.IOException;

public interface ReceiptPrintingService {

    String generateReceipt(Order order) throws TemplateException, IOException;

}
