package com.codetest.receiptprinter.domains;


import lombok.Data;

import java.math.BigDecimal;

@Data
public class ReceiptSummaryTmp {

    private BigDecimal subTotal;
    private BigDecimal taxAppliedTotal;

    public static ReceiptSummaryTmp newInstance() {
        ReceiptSummaryTmp instance = new ReceiptSummaryTmp();
        instance.subTotal = BigDecimal.ZERO;
        instance.taxAppliedTotal = BigDecimal.ZERO;
        return instance;
    }



    private ReceiptSummaryTmp() {
    }

}
