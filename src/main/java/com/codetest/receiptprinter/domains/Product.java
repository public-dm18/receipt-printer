package com.codetest.receiptprinter.domains;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class Product {
    private String name;
    private String type;
    private BigDecimal price;


}
