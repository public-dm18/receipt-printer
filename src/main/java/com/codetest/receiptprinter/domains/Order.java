package com.codetest.receiptprinter.domains;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class Order {
    private List<OrderedItem> items;
    private String purchaseLocation;
}
