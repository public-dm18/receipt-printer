package com.codetest.receiptprinter.domains;


import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
@AllArgsConstructor
public class Receipt {


    private List<OrderedItem> orderedItems;
    private BigDecimal subTotal;
    private BigDecimal tax;
    private BigDecimal total;


}
