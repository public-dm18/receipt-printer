package com.codetest.receiptprinter.configs;


import freemarker.template.Configuration;
import freemarker.template.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.util.ResourceUtils;

import java.io.IOException;

@org.springframework.context.annotation.Configuration
public class FreemakerEnv {


    @Bean
    public Configuration getFreemakerConfiguration() throws IOException {
        Configuration configuration = new Configuration(Configuration.VERSION_2_3_32);
        configuration.setDirectoryForTemplateLoading(ResourceUtils.getFile("src/main/resources/templates/"));
        configuration.setDefaultEncoding("utf-8");
        return configuration;
    }

    @Bean
    public Template getReceiptHTMLTableTemplate(@Autowired Configuration freemakerConfiguration) throws IOException {
        return freemakerConfiguration.getTemplate("receipt.ftl");
    }
}
