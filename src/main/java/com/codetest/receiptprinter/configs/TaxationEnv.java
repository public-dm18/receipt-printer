package com.codetest.receiptprinter.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Configuration
public class TaxationEnv {


    @Bean
    Map<String, Set<String>> getTaxExemptProductSetByLocationMap(){

        // init Env
        Map<String, Set<String>> map = new HashMap<>();

        map.put("CA", Set.of("food"));
        map.put("NY", Set.of("food", "clothing"));

        return map;
    }

    @Bean
    Map<String, BigDecimal> getTaxRateByLocationMap(){
        Map<String, BigDecimal> map = new HashMap<>();

        map.put("CA", new BigDecimal("0.0975"));
        map.put("NY", new BigDecimal("0.08875"));


        return map;
    }


}
