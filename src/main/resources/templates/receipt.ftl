<#ftl attributes={"content_type":"text/html; charset=UTF-8"}>
<#setting number_format="0.00">
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Receipt</title>
</head>
<body>
<style>
  .double-height-row {
    height: 3em;
  }
  .right { text-align: right; }
  .left { text-align: left; }

  td:first-child {
    width: 80px;
  }

  td:not(:first-child) {
    width: 100px;
  }
</style>
<table >
    <tr class="double-height-row"  >
        <td  class="left">item</td>
        <td  class="right">price</td>
        <td  class="right">qty</td>
    </tr>
<#list orderedItems as it>
    <tr  >
        <td class="left">${it.product.name}</td>
        <td class="right">${it.product.price?string("currency")}</td>
        <td class="right">${it.quantity?string("0") } </td>
    </tr>
</#list>
    <tr>
        <td colspan="2">subTotal:</td>
        <td class="right">${subTotal?string("currency")}</td>
    </tr>
    <tr>
        <td colspan="2">tax:</td>
        <td class="right">${tax?string("currency")}</td>
    </tr>
    <tr>
        <td colspan="2">total:</td>
        <td class="right">${total?string("currency")}</td>
    </tr>
</table>
</body>
</html>
